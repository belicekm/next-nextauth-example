const fs = require('fs');

export interface User {
    id?: string;
    name: string;
    email: string;
    password: string;
}

export async function createUser(user: User) {
    const users = await getUsers();
    user.id = `${Date.now()}`;
    users.push(user);

    return new Promise((resolve) => {
        fs.writeFile('data/users.json', JSON.stringify(users, null, 4), (err: any) => {
            if (err) {
                throw(err);
            }
            resolve(user);
        });
    });
}

export async function getUser(user: User) {
    const users = await getUsers();
    return users.find(({email, password}) => email === user?.email && password === user?.password);
}

export async function getUsers(): Promise<User[]> {
    return new Promise((resolve) => {
        fs.readFile('data/users.json', (err: any, data: string) => {
            const users: User[] = JSON.parse(data || '[]') as User[];
            resolve(users);
        });
    });
}
