import {signIn, signOut, useSession} from "next-auth/react";

export default function User() {
    const {data: session, status} = useSession();

    if (session) {
        return (
            <>
                Signed in as {session?.user?.email} <br/>
                <button onClick={() => signOut()}>Sign out</button>
            </>
        )
    } else if (status === 'loading') {
        return (<>Loading...</>);
    } else {
        return (
            <>
                Not signed in <br/>
                <button onClick={() => signIn()}>Sign in</button>
            </>
        )
    }
}
