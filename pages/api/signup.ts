import {createUser} from '../../services/user'
import {NextApiRequest, NextApiResponse} from "next";

export default async function signup(req: NextApiRequest, res: NextApiResponse) {
    try {
        const user = await createUser(req.body);
        res.status(200).json(user);
    } catch (error: any) {
        res.status(500).end(error.message);
    }
}
