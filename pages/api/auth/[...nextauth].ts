import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials";
import {getUser, User} from "../../../services/user";

export default NextAuth({
    pages: {
        signIn: '/login',
    },
    providers: [
        CredentialsProvider({
            name: 'Credentials',
            credentials: {
                email: {label: "Email", type: "email"},
                password: {label: "Password", type: "password"},
            },
            async authorize(credentials, req) {
                const user = await getUser(credentials as User);

                if (user) {
                    return user;
                } else {
                    return null;
                }
            }
        })
    ],
})
