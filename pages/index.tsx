import {signIn, signOut, useSession} from "next-auth/react";
import {useRouter} from "next/router";

export default function Home() {
    const {data: session} = useSession()
    const router = useRouter();
    if (session) {
        return (
            <>
                Signed in as {session?.user?.email} <br/>
                <button onClick={() => signOut()}>Sign out</button>
            </>
        )
    }
    return (
        <>
            Not signed in <br/>
            <button onClick={() => signIn()}>Sign in</button>
            <button onClick={() => router.push('register')}>Register</button>
        </>
    )
}
