import {User} from "../services/user";
import {signIn} from "next-auth/react";

export default function Login() {
    const handleLogin = async (event: any) => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const newUser: Partial<User> = {
            email: String(formData.get('email')) || '',
            password: String(formData.get('password')) || '',
        };

        if (newUser.email && newUser.password) {
            signIn('credentials', {
                ...newUser,
                callbackUrl: '/user'
            });
        }
    }

    return (
        <>
            Login <br/>
            <form onSubmit={handleLogin}>
                <input type={"email"} name={"email"}/><br/>
                <input type={"password"} name={"password"}/>
                <button>Login</button>
            </form>
        </>
    )
}
