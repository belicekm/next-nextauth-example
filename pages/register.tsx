import {User} from "../services/user";
import {signIn} from "next-auth/react";

export default function Register() {
    const handleRegister = async (event: any) => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const newUser: User = {
            email: String(formData.get('email')) || '',
            name: String(formData.get('name')) || '',
            password: String(formData.get('password')) || '',
        };

        if (newUser.name && newUser.email && newUser.password) {
            const res = await fetch('/api/signup', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(newUser),
            })
            const user: User = await res.json();
            if (res.status === 200) {
                signIn('credentials', {
                    ...user,
                    callbackUrl: '/new-user'
                });
            }
        }
    }

    return (
        <>
            Register <br/>
            <form onSubmit={handleRegister}>
                <input type={"text"} name={"name"}/><br/>
                <input type={"email"} name={"email"}/><br/>
                <input type={"password"} name={"password"}/>
                <button>Do register</button>
            </form>
        </>
    )
}
